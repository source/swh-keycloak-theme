<!--
  Copyright (C) 2020  The Software Heritage developers
  See the AUTHORS file at the top-level directory of this distribution
  License: GNU Affero General Public License version 3, or any later version
  See top-level LICENSE file for more information
  -->

<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=true; section>
  <#if section = "title">
    ${msg("emailForgotTitle")}
  <#elseif section = "header">
    ${msg("emailForgotTitle")}
  <#elseif section = "form">
    <form id="kc-reset-password-form" class="form reset-password ${properties.kcFormClass!}"
          action="${url.loginAction}" method="post">
        <div class="reset-password-field ${properties.kcFormGroupClass!}">
          <div class="${properties.kcLabelWrapperClass!}">
            <label for="username" class="${properties.kcLabelClass!}">
              <#if !realm.loginWithEmailAllowed>
                ${msg("username")}
              <#elseif !realm.registrationEmailAsUsername>
                ${msg("usernameOrEmail")}
              <#else>
                ${msg("email")}
              </#if>
            </label>
          </div>
          <div class="${properties.kcInputWrapperClass!}">
            <input type="text" id="username" name="username"
                   class="form-control ${properties.kcInputClass!}" autofocus/>
          </div>
        </div>

        <div class="${properties.kcFormGroupClass!} row mt-1">
          <div id="kc-form-options" class="${properties.kcFormOptionsClass!} col-8">
            <div class="${properties.kcFormOptionsWrapperClass!}">
              <a class="btn btn-default btn-block" href="${url.loginUrl}">
                <i class="mdi mdi-chevron-double-left"></i>${msg("backToLogin")}
              </a>
            </div>
          </div>
          <div id="kc-form-buttons" class="${properties.kcFormButtonsClass!} col-4">
            <input class="btn btn-primary btn-block" type="submit" value="${msg("doSubmit")}"/>
          </div>
        </div>
    </form>
  <#elseif section = "info" >
    <hr />
    ${msg("emailInstruction")}
  </#if>
</@layout.registrationLayout>
