<!--
  Copyright (C) 2020  The Software Heritage developers
  See the AUTHORS file at the top-level directory of this distribution
  License: GNU Affero General Public License version 3, or any later version
  See top-level LICENSE file for more information
  -->

<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
  <#if section = "header">
    ${msg("registerTitle")}
  <#elseif section = "form">
    <form id="kc-register-form" class="${properties.kcFormClass!}"
          action="${url.registrationAction}" method="post">
      <div class="mb-2 input-group ${messagesPerField.printIfExists('firstName',properties.kcFormGroupErrorClass!)}">
        <input type="text" id="firstName" class="form-control ${properties.kcInputClass!}"
                name="firstName" value="${(register.formData.firstName!'')}"
                placeholder="${msg("firstName")}" />
        <div class="input-group-append">
          <div class="input-group-text">
            <i class="mdi mdi-account-details"></i>
          </div>
        </div>
      </div>

      <div class="mb-2 input-group ${messagesPerField.printIfExists('lastName',properties.kcFormGroupErrorClass!)}">
        <input type="text" id="lastName" class="form-control ${properties.kcInputClass!}"
                name="lastName" value="${(register.formData.lastName!'')}"
                placeholder="${msg("lastName")}" />
        <div class="input-group-append">
          <div class="input-group-text">
            <i class="mdi mdi-account-details"></i>
          </div>
        </div>
      </div>

      <div class="mb-2 input-group ${messagesPerField.printIfExists('email',properties.kcFormGroupErrorClass!)}">
        <input type="text" id="email" class="form-control ${properties.kcInputClass!}"
                name="email" value="${(register.formData.email!'')}" autocomplete="email"
                placeholder="${msg("email")}" />
        <div class="input-group-append">
          <div class="input-group-text">
            <i class="mdi mdi-email"></i>
          </div>
        </div>
      </div>

      <#if !realm.registrationEmailAsUsername>
        <div class="mb-2 input-group ${messagesPerField.printIfExists('username',properties.kcFormGroupErrorClass!)}">
          <input type="text" id="username" class="form-control ${properties.kcInputClass!}"
                  name="username" value="${(register.formData.username!'')}"
                  autocomplete="username" placeholder="${msg("username")}" />
          <div class="input-group-append">
            <div class="input-group-text">
              <i class="mdi mdi-account"></i>
            </div>
          </div>
        </div>
      </#if>

      <#if passwordRequired>
        <div class="mb-2 input-group ${messagesPerField.printIfExists('password',properties.kcFormGroupErrorClass!)}">
          <input type="password" id="password" class="form-control ${properties.kcInputClass!}"
                  name="password" autocomplete="new-password" placeholder="${msg("password")}" />
          <div class="input-group-append">
            <div class="input-group-text">
              <i class="mdi mdi-lock"></i>
            </div>
          </div>
        </div>

        <div class="mb-2 input-group ${messagesPerField.printIfExists('password-confirm',properties.kcFormGroupErrorClass!)}">
          <input type="password" id="password-confirm" class="form-control ${properties.kcInputClass!}"
                  name="password-confirm" placeholder="${msg("passwordConfirm")}" />
          <div class="input-group-append">
            <div class="input-group-text">
              <i class="mdi mdi-lock"></i>
            </div>
          </div>
        </div>
      </#if>

      <#if recaptchaRequired??>
        <div class="text-center">
          <div class="g-recaptcha d-inline-block" data-sitekey="${recaptchaSiteKey}"></div>
        </div>
      </#if>

      <div class="row mt-3">
        <div id="kc-form-options" class="${properties.kcFormOptionsClass!} col-8">
          <div class="${properties.kcFormOptionsWrapperClass!}">
            <a class="btn btn-default btn-block" href="${url.loginUrl}">
              <i class="mdi mdi-chevron-double-left"></i>${msg("backToLogin")}
            </a>
          </div>
        </div>

        <div id="kc-form-buttons" class="${properties.kcFormButtonsClass!} col-4">
          <input class="btn btn-primary btn-block"
                  type="submit" value="${msg("doRegister")}"/>
        </div>
      </div>
    </form>
  </#if>
</@layout.registrationLayout>
