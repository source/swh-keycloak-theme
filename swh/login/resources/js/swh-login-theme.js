/**
 * Copyright (C) 2020  The Software Heritage developers
 * See the AUTHORS file at the top-level directory of this distribution
 * License: GNU Affero General Public License version 3, or any later version
 * See top-level LICENSE file for more information
 */

// when a new language is selected, redirect to the language's page
function languageSelected() {
  var redirectUrl = $('#language-picker-dropdown').val();
  window.location.href = redirectUrl;
}