<!--
  Copyright (C) 2020  The Software Heritage developers
  See the AUTHORS file at the top-level directory of this distribution
  License: GNU Affero General Public License version 3, or any later version
  See top-level LICENSE file for more information
  -->

<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=true; section>
  <#if section = "title">
    ${msg("updatePasswordTitle")}
  <#elseif section = "header">
    ${msg("updatePasswordTitle")}
  <#elseif section = "form">
    <form id="kc-passwd-update-form" class="form update-password ${properties.kcFormClass!}"
          action="${url.loginAction}" method="post">
      <div class="mb-3 update-password-field ${properties.kcFormGroupClass!}">
        <div class="${properties.kcInputWrapperClass!}">
          <input type="password" id="password-new" name="password-new"
                 class="form-control ${properties.kcInputClass!}"
                 placeholder="${msg("passwordNew")}"
                 autofocus autocomplete="off" />
        </div>
      </div>

      <div class="mb-3 update-password-field ${properties.kcFormGroupClass!}">
        <div class="${properties.kcInputWrapperClass!}">
          <input type="password" id="password-confirm" name="password-confirm"
                 class="form-control ${properties.kcInputClass!}"
                 placeholder="${msg("passwordConfirm")}"
                 autocomplete="off" />
        </div>
      </div>

      <div class="${properties.kcFormGroupClass!} row update-password-button-container">
        <div id="kc-form-options" class="${properties.kcFormOptionsClass!} col-6">
          <div class="${properties.kcFormOptionsWrapperClass!}">
          </div>
        </div>

        <div id="kc-form-buttons" class="${properties.kcFormButtonsClass!} col-6">
          <input class="btn btn-primary btn-block" type="submit" value="${msg("doSubmit")}"/>
        </div>
      </div>
    </form>
  </#if>
</@layout.registrationLayout>